#define _POSIX_C_SOURCE 200112L
#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>


struct image* bmp_to_img(FILE* file){
	struct bmp_header bmp_hdr;
	if (fread(&bmp_hdr, 1, sizeof(struct bmp_header), file) != sizeof(struct bmp_header)){
		return NULL;
	}

	struct image* img = (struct image*)malloc(sizeof(struct image));

	img->width = bmp_hdr.biWidth;
	img->height = bmp_hdr.biHeight;

	uint32_t padpytes = ((img->width*3)%4 != 0) * (4 - (img->width * 3)%4);

	if(sizeof(struct bmp_header) != bmp_hdr.bOffBits){
		fseek(file, bmp_hdr.bOffBits - (uint32_t)sizeof(struct bmp_header), SEEK_CUR);
	}

	img->pixel_data = (struct pixel*)malloc(img->width*img->height*3);

	struct pixel* read_pix = img->pixel_data;
	for(int i = 0; i < img->height; i++){
		fread(read_pix, 1, 3*img->width, file);
		read_pix += img->width;
		if(padpytes){
			fseek(file, padpytes, SEEK_CUR);
		}
	}
	fseek(file, 0, SEEK_SET);

	return img;
}


void img_to_bmp(struct image* img, FILE* file){
	fseek(file, 0, SEEK_SET);

	struct bmp_header bmp_hdr;
	fread(&bmp_hdr, 1, sizeof(struct bmp_header), file);
	if(img->width != bmp_hdr.biWidth || img->height != bmp_hdr.biHeight){
		return;
	}

	uint32_t padpytes = ((img->width*3)%4 != 0) * (4 - (img->width * 3)%4);

	if(sizeof(struct bmp_header) != bmp_hdr.bOffBits){
		fseek(file, bmp_hdr.bOffBits - (uint32_t)sizeof(struct bmp_header), SEEK_CUR);
	}

	struct pixel* read_pix = img->pixel_data;
	for(int i = 0; i < img->height; i++){
		fwrite(read_pix, 1, 3*img->width, file);
		read_pix += img->width;
		if(padpytes){
			fseek(file, padpytes, SEEK_CUR);
		}
	}

}

void create_bmp(FILE* ifile, FILE* ofile){
	struct bmp_header bmp_hdr;
	fread(&bmp_hdr, sizeof(struct bmp_header), 1, ifile);

	uint32_t tmp = bmp_hdr.biWidth;
	uint32_t padded_width = bmp_hdr.biHeight*3;

	if(padded_width % 4){
		padded_width += (4 - padded_width % 4);
	}

	fflush(ofile);
	ftruncate(ofile->_fileno, bmp_hdr.biWidth*padded_width+bmp_hdr.bOffBits);

	bmp_hdr.bfileSize = bmp_hdr.biWidth*padded_width+bmp_hdr.bOffBits;

	bmp_hdr.biWidth = bmp_hdr.biHeight;
	bmp_hdr.biHeight = tmp;

	bmp_hdr.biSizeImage = padded_width * bmp_hdr.biHeight;
	bmp_hdr.biXPelsPerMeter = 0;
	bmp_hdr.biYPelsPerMeter = 0;
	
	fwrite(&bmp_hdr, sizeof(struct bmp_header), 1, ofile);


	fseek(ofile, 0, SEEK_SET);
}
