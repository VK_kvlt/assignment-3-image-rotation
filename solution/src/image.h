#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
        uint32_t width;
        uint32_t height;
        struct pixel* pixel_data;
};

void turn_img(struct image* input_image, struct image* output_image);

