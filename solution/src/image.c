#include "image.h"
#include <stdint.h>

void turn_img(struct image* input_image, struct image* output_image){
	void* src = (*input_image).pixel_data;
	void* dest = (*output_image).pixel_data;
	uint32_t cntx = 0;
	uint32_t cnty = 0;
	for(;;){
		*((struct pixel*)((char*)dest + (cntx)*(*output_image).width*3 + ((*output_image).width - 1 -cnty)*3)) = *((struct pixel*)src);
		src = ((char*)src)+3;
		cntx++;
		if(cntx == (*input_image).width){
			cntx = 0;
			cnty++;
			if(cnty == (*input_image).height){
				break;
			}
		}
	}
}
