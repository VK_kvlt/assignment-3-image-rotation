#include "image.h"
#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
	if (argc != 3){
		return 1;
	}
	FILE* input_file = fopen(argv[1], "r");
	FILE* output_file = fopen(argv[2], "w+");
	struct image* input_image = bmp_to_img(input_file);
	create_bmp(input_file, output_file);
	struct image* output_image = bmp_to_img(output_file);
	turn_img(input_image, output_image);
	img_to_bmp(output_image, output_file);
	free(input_image->pixel_data);
	free(input_image);
	free(output_image->pixel_data);
	free(output_image);
	fclose(input_file);
	fclose(output_file);
	
	return 0;
}
